extends Spatial
# This is the base class for OSC communication with SATIE. A script should extend it with any
# custom additions according to SATIE objects instances control. See example

signal node_connected(node_id)

var node_type = "source"
var node_id = "";
var node_group = "default"
var node_def_name = ""
var OSC_ip = "127.0.0.1"
var OSC_port = 18032

var target = get_parent()
var gain = 100
var trim_db = 0
var use_custom_update = false
var auto_update = true
var trigger = false
var velocity
var aed
var distance_vector
var _previous_distance = 0
var updating = false
var distance_scale = 1
var distance_effect = 1.5

var oscsender

func _init(id: String="", def_name: String="", group: String="default", type: String="source"):
	node_id = id
	node_def_name = def_name
	node_group = group
	node_type = type
	oscsender = OSCsender.new()

func node_connect():
	if node_id.empty():
		print("SatieOSC -> node_id cannot be empty")
		return
	elif node_def_name.empty():
		print("SatieOSC -> node_id cannot be empty")
		return
	else:
		oscsender.init(OSC_ip, OSC_port)		# will send messages to ip:port
		oscsender.start()
	match node_type:
		"source":
			create_source()
		"effect":
			create_effect(0)
		"process":
			create_process()
	emit_signal("node_connected", node_id)

func create_source():
	oscsender.msg_address("/satie/scene/createSource")
	oscsender.msg_add_string(node_id)
	oscsender.msg_add_string(node_def_name)
	oscsender.msg_add_string(node_group)
	oscsender.msg_send()

func create_effect(aux=0):
	oscsender.msg_address("/satie/scene/createEffect")
	oscsender.msg_add_string(node_id)
	oscsender.msg_add_string(node_def_name)
	oscsender.msg_add_string(node_group)
	oscsender.msg_add_int(aux)
	oscsender.msg_send()

func create_process():
	oscsender.msg_address("/satie/scene/createProcess")
	oscsender.msg_add_string(node_id)
	oscsender.msg_add_string(node_def_name)
	# oscsender.add(node_group)
	oscsender.msg_send()

func delete_node():
	oscsender.msg_address("/satie/scene/deleteNode")
	oscsender.msg_add_string(node_id)
	oscsender.msg_send()

func set_source(args):
	set_source_node(args)

func set_source_node(args: Array):
	""" args: list of string, other type values"""
	oscsender.msg_address("/satie/source/set")
	oscsender.msg_add_string(node_id)
	for item in args:
		oscsender.add(item)
	oscsender.msg_send()

func set_process_node(dico: Dictionary):
	if node_type == "process":
		oscsender.msg_address("/satie/process/set")
		oscsender.msg_add_string(node_id)
		for k in dico.keys():
			oscsender.msg_add_string(k)
			oscsender.add(dico[k])
		oscsender.msg_send()
	else:
		print("set_process_node ->", self.node_id, " node is not of type process, ignoring")

func set_process_property(dico: Dictionary):
	if node_type == "process":
		oscsender.msg_address("/satie/process/property")
		oscsender.msg_add_string(node_id)
		for k in dico.keys():
			oscsender.msg_add_string(k)
			oscsender.add(dico[k])
		oscsender.msg_send()
	else:
		print("set_process_property ->", self.node_id, " node is not of type process, ignoring")

func eval_process_func(handler: String, args=[]):
	if node_type == "process":
		oscsender.msg_address("/satie/process/eval")
		oscsender.msg_add_string(node_id)
		oscsender.msg_add_string(handler)
		if args.size() > 0:
			for i in args:
				oscsender.add(i)
		oscsender.msg_send()
	else:
		print("eval_process_func ->", self.node_id, " node is not of type process, ignoring")

func _process(delta):
	if auto_update:
		if use_custom_update:
			custom_update()
		else:
			distance_vector = self.global_transform.origin - target.global_transform.origin
			var distance = distance_vector.length()
			if distance != _previous_distance:
				updating = true
				aed = cartesian_2_spherical(distance_vector)
				gain = gain_from_distance(aed[2]) + trim_db
				# if node_type != "process":
				update(aed[0], aed[1], gain)
				_previous_distance = distance
			else:
				updating = false

func custom_update():
	aed = cartesian_2_spherical(target.translation)
	velocity = abs(target.angular_velocity.y * 100)
	if velocity > 20:
		gain = gain_from_distance(10.0/velocity)
		trigger = true
	else:
		gain = gain_from_distance(0.5)
		trigger = true
	if velocity == 0.0:
		trigger = false

	update(aed[0], aed[1], gain)

func update(azi, ele, gain):
	oscsender.msg_address("/satie/source/update")
	oscsender.add(node_id)
	oscsender.add(azi)
	oscsender.add(ele)
	oscsender.add(gain)
	oscsender.add(0)
	oscsender.add(15000)
	oscsender.msg_send()

func update_sample_rate():
	oscsender.msg_address("/satie/source/set")
	oscsender.add(node_id)
	oscsender.add("rate")
	oscsender.add(randf() - 0.5)
	oscsender.msg_send()

func gain_from_distance(distance):
	"""
	Calculate gain from distance
	:param distance:
	:return:
	"""
	# var g = log(distance_to_attenuation(distance)) * 20
	var g = linear2db(distance_to_attenuation(pow(distance, distance_effect)))
	return  g

func distance_to_attenuation(distance):
	"""
	A very crude distance to attenuation conversion.
	(we use the abs of the number you give us.)
	:param distance: Absolute distance in meters.
	:return:
	"""
	var d = abs(distance * distance_scale)
	if d<= 1.0:
		return 1.0
	else:
		return 1.0 / d


func cartesian_2_spherical(xyz):
	"""
	Converts XYZ to AED.
	:param xyz: list of three floats
	:return: tuple of three floats
	"""
	var x = xyz[0]
	var y = xyz[1]
	var z = xyz[2]

	var elevation

	var distance = sqrt((x * x) + (y * y) + (z * z))
	var azimuth = atan2(z, x) - PI / 2
	# put in range of [-pi, pi]
	if azimuth > PI:
		azimuth -= 2 * PI
	elif azimuth < -PI:
		azimuth += 2 * PI
	if distance > 0.0000001:
		elevation = asin(y / distance)
	else:
		elevation = 0.0

	return [rad2deg(-azimuth), rad2deg(elevation), distance]

func distance_between_vectors(vec1, vec2):
	var x = vec2[0] - vec1[0]
	var y = vec2[1] - vec1[1]
	var z = vec2[2] - vec1[2]

	return sqrt((x*x) + (y*y) + (z*z))

func _exit_tree ( ):
	delete_node()
	oscsender.stop()
