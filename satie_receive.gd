
extends OSCreceiver

signal analysis(hasFreq, amp, freq)

func _ready():
	set_process(true) # OSCreceiver module uses _process loop do its thing
	connect("osc_message_received", self, "_on_msg")

func process_analysis(msg):
	if ( msg.empty() ):
		print(self, " message was empty")
		return
	emit_signal("analysis", msg.arg(1), msg.arg(2), msg.arg(3), msg.arg(4))

func _on_msg(msg):
	if msg.address() == "/analysis":
			process_analysis(msg)
